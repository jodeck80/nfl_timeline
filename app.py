import json

from flask import Flask, render_template, request, jsonify
import pandas as pd

from bson.json_util import dumps

from python.NFLMongoDBConnector import NFLMongoDBConnector
from python.InvalidUsage import InvalidUsage

# data to return
chart_data_full = {}
chart_data_short = {}

app = Flask(__name__)

# fields correspond to columns in csv
fields = [0, 2, 3, 5, 9]
city_map = {"FUQUAY VARINA": ["Fuquay-Varina", "FUQUAY VARINA", "FV",
                              "FUQUAY-VARINA", "FUQUAY VARINA ",
                              " FUQUAY VARINA"],
            "RDU": ["RDU", "RDU AIRPORT"],
            "RALEIGH": ["RALEIGH", " RALEIGH", "RALEIGH ", "27615"],
            "CARY": ["CARY", " CARY", "CARY "],
            "APEX": ["APEX", " APEX", "APEX "],
            "RTP": ["RTP", "RESEARCH TRIANGLE PARK"],
            "MORRISVILLE": ["MORRISVILLE", "MORRISVILE",
                            " MORRISVILLE", "MORRISVILLE "],
            "HOLLY SPRINGS": ["HOLLY SPRINGS", " HOLLY SPRINGS",
                              "HOLLY SPRING", "HOLLY SPRINGS "],
            "WAKE FOREST": ["WAKE FOREST", " WAKE FOREST", "WAKE FOREST "]}


def create_app():
    # init csv data
    init_data()

    # run app
    app.run(debug=True, port=5010)


def init_data():
    # read in the data
    global chart_data_full
    chart_data_full = read_data('../../data/Restaurants_in_Wake_County.csv')

    # read in the short data
    global chart_data_short
    chart_data_short = read_data('../../data/Restaurants_in_Wake_County_short.csv')

    print "INIT COMPLETE!"


def read_data(csv_file):
    # read in the data
    df = pd.read_csv(csv_file, skipinitialspace=True,
                     usecols=fields, encoding='utf-8-sig')

    # See the keys
    print df.keys()

    # convert from data frame to dictionary
    chart_data = df.to_dict(orient='records')

    city_list = set()

    # get only date of date/time (first 10 places of string)
    for sub in chart_data:
        sub["RESTAURANTOPENDATE"] = sub["RESTAURANTOPENDATE"][:10]
        sub["CITY"] = sub["CITY"].upper()
        for key, value in city_map.iteritems():
            if str(sub["CITY"]) in value:
                sub["CITY"] = key

        if sub["CITY"] in city_list:
            pass
        else:
            city_list.add(sub["CITY"])
        # sub["OBJECTID"] = sub[0].encode('utf-8')

    return chart_data


@app.route("/")
def index():
    # translate into json
    chart_data = json.dumps(chart_data_full, indent=2)

    # prepare to pass over to client
    data = {'chart_data': chart_data}

    return render_template("index.html", data=data)


@app.route("/short")
def short():
    # translate into json
    chart_data = json.dumps(chart_data_short, indent=2)

    # prepare to pass over to client
    data = {'chart_data': chart_data}

    return render_template("short_page.html", data=data)


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route("/chart")
def chart():
    # check if year is selected
    year = request.args.get('year')
    if year is None:
        raise InvalidUsage('Please provide a valid year parameter',
                           status_code=410)

    # check if team is selected
    team = request.args.get('team')
    if team is None:
        raise InvalidUsage('Please provide a valid team parameter',
                           status_code=410)

    print year
    # open new connection
    db_connection = NFLMongoDBConnector()

    # get player data for selected year
    test = db_connection.get_year(year, team)

    # convert from bson to json
    chart_data = dumps(test)

    # prepare to pass over to client
    data = {'chart_data': chart_data, 'year': year, 'team': team}

    return render_template("chart.html", data=data)

if __name__ == "__main__":
    create_app()
