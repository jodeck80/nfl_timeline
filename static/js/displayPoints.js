    //var datapoints = {{ data.chart_data | safe }}
    var city_list = 0
    //console.log(datapoints);

    // Parse the date / time
    var parseDate = d3.time.format("%Y-%m-%d").parse;

    // Display time
    var formatTime = d3.timeFormat("%B %d, %Y");

    var margin = {top: 60, right: 20, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

    var x = d3.time.scale()
        .range([0, width]);

    var y = d3.scale.linear()
        .range([height, 0]);

    var color = d3.scale.category10();

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .ticks(5);

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .ticks(5);

    var svg = d3.select("body").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // display additional info in tooltip on mouseover
    var tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function(d) {
        return "<strong>Name:</strong> <span style='color:red'>" + d.NAME + "</span> <br>" +
                "<strong>Open Date:</strong> <span style='color:red'>" + formatTime(d.RESTAURANTOPENDATE)+ "</span> <br>" +
                "<strong>City:</strong> <span style='color:red'>" + d.CITY + "</span> <br>" + 
                "<strong>Address:</strong> <span style='color:red'>" + d.ADDRESS1 + "</span>" ;
      })

    var dropDown = d3.select("#filter").append("select")
                    .attr("name", "country-list");

    svg.call(tip);

    function draw(data, city_list){
      data.forEach(function(d) {
        d.RESTAURANTOPENDATE = parseDate(d.RESTAURANTOPENDATE);
        d.id = +d.OBJECTID;
        d.city = d.CITY;
      });

      x.domain(d3.extent(data, function(d) { return d.RESTAURANTOPENDATE; })).nice;
      y.domain(d3.extent(data, function(d) { return d.OBJECTID; })).nice();

      svg.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxis)
        .append("text")
          .attr("class", "label")
          .attr("x", width)
          .attr("y", -6)
          .style("text-anchor", "end")
          .text("Open Year");

      svg.append("g")
          .attr("class", "y axis")
          .call(yAxis)
        .append("text")
          .attr("class", "label")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", ".71em")
          .style("text-anchor", "end")
          .text("ID")

      svg.selectAll(".dot")
          .data(data)
        .enter().append("circle")
          .attr("class", "dot")
          .attr("r", 3.5)
          .attr("cx", function(d) { return x(d.RESTAURANTOPENDATE); })
          .attr("cy", function(d) { return y(d.OBJECTID); })
          .style("fill", function(d) { return color(d.city); })
          .on('mouseover', function(d, i) {
              //console.log("mouseover on", this);
              // make the mouseover'd element
              // bigger
              d3.select(this)
                .transition()
                .duration(100)
                .attr('r', 20)
                .attr('fill', '#ff0000');
                
                // display tooltip                
                tip.show(d); 
          })
          .on('mouseout', function(d, i) {
              //console.log(d.NAME);
              // return the mouseover'd element
              // to being smaller
              d3.select(this)
                .transition()
                .duration(100)
                .attr('r', 3.5)
                .attr('fill', '#000000');
            
                // hide tooltip
                tip.hide(d);
            });

/*
	// the legend color guide
	var legend = svg.selectAll("rect")
			.data(color.domain())
		.enter().append("rect")
		.attr({
		  x: function(d, i) { return (40 + i*80); },
		  y: height,
		  width: 25,
		  height: 12
		})
		.style("fill", function(d) { return color(d); });


	// legend labels	
		svg.selectAll("text")
			.data()
		.enter().append("text")
		.attr({
		x: function(d, i) { return (40 + i*80); },
		y: height + 24,
		})
		.text(function(d) { return d; });
*/

    var options = dropDown.selectAll("option")
               .data(d3.map(data, function(d){return d.city;}).keys())
               //.data(data)
               .enter()
               .append("option");

    options.text(function (d) { return d; })
           .attr("value", function (d) { return d; });

    dropDown.on("change", function() {
          var selected = this.value;
          displayOthers = this.checked ? "inline" : "none";
          display = this.checked ? "none" : "inline";

          svg.selectAll(".dot")
              .filter(function(d) {return selected != d.city;})
              .attr("display", displayOthers);
              
          svg.selectAll(".dot")
              .filter(function(d) {return selected == d.city;})
              .attr("display", display);
          });
        }

draw(datapoints, city_list);
