//var datapoints = {{ data.chart_data | safe }};
var team = data.team;
var year = data.year;

// display year in html
document.getElementById("h1").innerHTML = String(year).concat(' ').concat(team);

var players = datapoints.map(function (i) {
    return i.player;
});

// convert years to list of ints
datapoints = datapoints.map(function(i){
    var o = Object.assign({}, i);
    o.years_int = i.years.map(Number);
    return o;
});

// get min first year and max last year for 
// timeline extents
var firstYears = datapoints.map(function (i) {
    return parseInt(i.firstYear);
});
var minFirstYear = Math.min.apply(Math, firstYears);

var lastYears = datapoints.map(function (i) {
    return parseInt(i.lastYear);
});
var maxLastYear = Math.max.apply(Math, lastYears);

var categories = []
categories = categories.concat(players)

// types used for color scale
types = ["2017", "2018"];

// Display time
var formatTime = d3.timeFormat("%B %d, %Y");

colorScale = d3.scale.ordinal()
    .domain(types)
    .range(["#96abb1", "#313746"]);

var grid = d3.range(25).map(function (i) {
    return { 'x1': 0, 'y1': 0, 'x2': 0, 'y2': 480 };
});

var margin = { top: 50, right: 200, bottom: 0, left: 40 },
    width = 1000 - margin.left - margin.right,
    height = 1000 - margin.top - margin.bottom;
var xscale = d3.scale.linear()
    .domain([minFirstYear, maxLastYear])
    .range([0, width]);

var yscale = d3.scale.linear()
    .domain([0, categories.length])
    .range([0, height]);

// Format as year. Ie, with no commas
var xAxis = d3.svg.axis();
xAxis
    .orient('bottom')
    .scale(xscale)
    .tickFormat(d3.format(""));

var yAxis = d3.svg.axis();
yAxis
    .orient('left')
    .scale(yscale)
    .tickSize(1)
    .tickFormat(function (d, i) { return categories[i]; })
    .tickValues(d3.range(categories.length));

// display additional info in tooltip on mouseover
var tip = d3.tip()
    .attr('class', 'd3-tip')
    .offset([50, 200])
    .html(function (d) {
        return "<strong>Name:</strong> <span style='color:red'>" + d.player + "</span> <br>" +
            "<strong>Start Date:</strong> <span style='color:red'>" + d.firstYear + "</span> <br>" +
            "<strong>End Date:</strong> <span style='color:red'>" + d.lastYear + "</span> <br>" +
            "<strong>College:</strong> <span style='color:red'>" + d.college + "</span>";
    })

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom);
svg.call(tip);

var timeline = d3.layout.timeline()
    .size([width, height])
    //.maxBandHeight(10)
    .bandStart(function (d) { return ((d.firstYear)) })
    .bandEnd(function (d) { return (d.lastYear) })
    .dateFormat(function (d) { return parseInt(d) })
    .extent([minFirstYear, maxLastYear])
timelineBands = timeline(datapoints);

d3.select("svg").selectAll("rect")
    .data(timelineBands)
    .enter()
    .append("rect")
    .attr("x", function (d) { return (d.start) })
    .attr("y", function (d) { return d.y })
    .attr("height", function (d) { return d.dy })
    .attr("width", function (d) { return d.end - d.start + 33 })
    .attr("transform", "translate(150,0)")
    //.attr("width", function (d) {return parseInt(d.lastYear) - parseInt(d.startYear)})
    //.style("fill", function (d) {return colorScale(d.lastYear)})
    .style("fill", "#687a97")
    .style("stroke", "black")
    .on('mouseover', function (d, i) {
        // display tooltip
        tip.show(d);
    })
    .on('mouseout', function (d, i) {
        // hide tooltip
        tip.hide(d);
    });

var y_xis = d3.select("svg").append('g')
    .attr("transform", "translate(140," + (10) + ")")
    .attr('id', 'yaxis')
    .call(yAxis);

var x_xis = d3.select("svg").append('g')
    .attr("transform", "translate(150," + (height + 10) + ")")
    .attr('id', 'xaxis')
    .call(xAxis);