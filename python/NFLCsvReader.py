# read roster information from csv files and return as dicitionary

import pandas as pd
# import csv


class NFLCsvReader:

    def read_data(self, csv_file):
        # read in the data
        df = pd.read_csv(csv_file)

        # rename columns
        df.rename(columns={'No.': 'Number',
                           'College/Univ': 'College',
                           'Drafted (tm/rnd/yr)': 'Drafted'},
                  inplace=True)

        print df.keys()

        # drop last row with team totals
        df.drop(df.tail(1).index, inplace=True)

        # Replace NaN drafted with Free Agent
        df.loc[df['Drafted'].isnull(), 'Drafted'] = 'FA'

        # remove identifier pro football reference uses
        # and removes * that denotes pro bowl
        df['Player'] = df['Player'].str.split('\\').str[0]
        df['Player'] = df['Player'].str.split('*').str[0]

        return df

    def do_work(self):
        print "Doing work!"

if __name__ == "__main__":
    dummy = NFLCsvReader()
    dummy.do_work()
    roster = dummy.read_data('/home/joe/Documents/apps/data/nfl_rosters/patriots/pats_2018_roster.csv')