from NFLCsvReader import NFLCsvReader
from NFLMongoDBConnector import NFLMongoDBConnector

from bson.json_util import dumps

if __name__ == "__main__":
    reader = NFLCsvReader()
    db_connection = NFLMongoDBConnector()

    years = ['2015']
    team = 'Chargers'
    data_dir = '../../data/nfl_rosters'

    for year in years:
        csv_file = data_dir + '/' + team.lower() + '/' + year + '_roster.csv'
        roster = reader.read_data(csv_file)

        for index, row in roster.iterrows():
            db_connection.insert_player(row, year, team)
        print "Ingested: " + str(csv_file)
