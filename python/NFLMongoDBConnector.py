# import csv of team into database
# csv is downloaded from pro-football reference
from pymongo import MongoClient
import pymongo
import pandas as pd


class NFLMongoDBConnector:
    def __init__(self):
        # use default parameters, assumes db on localhost:27017
        client = MongoClient()
        self.db = client.nfl_rosters
        self.team_tables = {
            'Patriots': 'players',
            'Chargers': 'players_chargers'
        }

    def insert_player(self, player, year, team):
        # player is dataframe. has these keys that are
        # unique regardless of year:
        # Name, birthdate, college, draft year, draft pos

        table_lookup = self.team_tables[team]
        table = self.db[table_lookup]
        # check to see if player exists
        player_db = table.find_one({"$and": [{"player": str(player["Player"])},
                                             {"birthdate": str(player["BirthDate"])}]})
        result = player_db

        # update list of years
        if player_db:
            if(('years' not in player_db) or (year not in player_db['years'])):
                new_year = {"$push": {"years": year }}
                result = table.update_one({"player": player["Player"]}, new_year)

        # update min, max years
        if player_db:
            if(year < player_db['firstYear']):
                newYear = {"$set": {"firstYear": year}}
                result = table.update_one({"player": player["Player"]}, newYear)
            elif(year > player_db['lastYear']):
                newYear = {"$set": {"lastYear": year}}
                result = table.update_one({"player": player["Player"]}, newYear)
        else:
            post_data = {
                'player': str(player["Player"]),
                'college': str(player["College"]),
                'birthdate': str(player["BirthDate"]),
                'drafted': str(player["Drafted"]),
                'firstYear': year,
                'lastYear': year,
                'years': [year]
            }
            print post_data
            result = table.insert_one(post_data)

        return result

    def insert_team(self):
        # insert a new team
        pass

        # insert player if doesnt already exist

    def get_year(self, year, team):
        table_lookup = self.team_tables[team]
        table = self.db[table_lookup]

        players = table.find({"years": {"$in": [year]}})
        players.sort([("firstYear", pymongo.ASCENDING), ("lastYear", pymongo.DESCENDING)])

        return players

    def get_team(self, team, year):
        # return a team for a given year
        pass

    def do_work(self):
        print "In do_work"

if __name__ == "__main__":
    dummy = NFLMongoDBConnector()
    dummy.do_work()
